package main

import (
	"encoding/csv"
	"fmt"
	"os"
	"regexp"
	"strconv"
	"strings"
	"time"
)

// создаем ГЛОБАЛЬНО структуру данных
type Entry struct {
	Name       string
	Surname    string
	Tel        string
	LastAccess string
}

var CSVFILE = "./csv.data" // путь до файла с данными (типа БД)
var data = []Entry{}       // создаем ГЛОБАЛЬНУЮ переменную типа СРЕЗ СТРУКТУР Entry
var index map[string]int   // объявляем переменную типа map (соварь) - ключ-строка: значение-инт (НЕ инициализируем значениями, просто ТИП)

// функция чтения CSV-файла
func readCSV(filepath string) error {
	_, err := os.Stat(filepath) // проверяем существование файла
	if err != nil {
		return err
	}

	f, err := os.Open(filepath) // открываем файл для работы с ним
	if err != nil {
		return err
	}
	defer f.Close()

	// CSV file read all at once
	lines, err := csv.NewReader(f).ReadAll()
	if err != nil {
		return err
	}

	for _, line := range lines {
		temp := Entry{
			Name:       line[0],
			Surname:    line[1],
			Tel:        line[2],
			LastAccess: line[3],
		}
		// сохраняем в ГЛОБАЛЬНУЮ переменную
		data = append(data, temp)
	}
	return nil
}

// функция записи в файл
func saveCSV(filepath string) error {
	csvfile, err := os.Create(filepath) // создаем файл
	if err != nil {
		return err
	}
	defer csvfile.Close()

	csvwriter := csv.NewWriter(csvfile)
	for _, row := range data {
		temp := []string{row.Name, row.Surname, row.Tel, row.LastAccess}
		_ = csvwriter.Write(temp)
	}
	csvwriter.Flush()
	return nil
}

// создаем ИНДЕКС записей в data
func createIndex() error {
	index = make(map[string]int)
	for i, k := range data {
		// fmt.Println(i) // в i лежит - индекс строки из data (0,1,2...)
		key := k.Tel
		index[key] = i
	}
	return nil
}

func validTel(key string) (string, error) {
	t := strings.ReplaceAll(key, "-", "") // проверяем введенный номер телефона на валидность
	//fmt.Println(matchTel(t))
	if !matchTel(t) {
		// fmt.Println("Not a valid tel number:", t) // если ф номере телефона есть буквы или НЕ только цифры - выводим подсказку и завершаемся
		return key, fmt.Errorf("not a valid number: %s", key)
	}
	return t, nil
}

// функция вывода на экран ВСЕХ записей из БД (data)
func list() {
	// fmt.Println(len(data))
	if len(data) == 0 {
		fmt.Println("Data is empty.")
		return
	}
	for _, v := range data {
		fmt.Println(v)
	}
}

func insert(pS *Entry) error {
	// If it already exists, do not add it
	_, ok := index[(*pS).Tel]
	if ok {
		return fmt.Errorf("%s already exists", pS.Tel)
	}
	data = append(data, *pS)
	// Update the index
	_ = createIndex()

	err := saveCSV(CSVFILE)
	if err != nil {
		return err
	}
	return nil
}

func deleteEntry(key string) error {
	i, ok := index[key]
	if !ok {
		return fmt.Errorf("%s cannot be found", key)
	}
	data = append(data[:i], data[i+1:]...)
	// Update the index - key does not exist any more
	delete(index, key)

	err := saveCSV(CSVFILE)
	if err != nil {
		return err
	}
	return nil
}

// функция поиска данных по номеру телефона
func search(key string) *Entry {
	i, ok := index[key]
	if !ok {
		return nil
	}
	data[i].LastAccess = strconv.FormatInt(time.Now().Unix(), 10)
	return &data[i]
}

// функция проверки валидности телефонного номера
func matchTel(s string) bool {
	t := []byte(s)
	re := regexp.MustCompile(`\d{11}$`)
	return re.Match(t)
}

// Initialized by the user – returns a pointer
// If it returns nil, there was an error
func initS(N, S, T string) *Entry {
	// Both of them should have a value
	if T == "" || S == "" {
		return nil
	}
	// Give LastAccess a value
	LastAccess := strconv.FormatInt(time.Now().Unix(), 10)
	return &Entry{Name: N, Surname: S, Tel: T, LastAccess: LastAccess}
}

// MAIN
func main() {
	fmt.Println("\n========== Phone Book App ==========")

	args := os.Args
	//fmt.Println(len(args))
	if len(args) == 1 {
		fmt.Println("Usage: search|list|insert|delete <arguments>")
		return
	}

	// если CSV-файл не существует - создаем новый
	fileinfo, err := os.Stat(CSVFILE)
	// если ошибка не равна nil - файл не существует
	if err != nil {
		fmt.Println("Creating", CSVFILE)
		f, err := os.Create(CSVFILE)
		if err != nil {
			f.Close()
			fmt.Println(err)
			return
		}
		f.Close()
	}

	//fileinfo, err := os.Stat(CSVFILE)
	mode := fileinfo.Mode() // тут проверяем тип файла - является ли он ОБЫЧНЫМ файлом UNIX
	//fmt.Println(mode.IsRegular()) // возвращает true если файл ОБЫЧНЫЙ
	if !mode.IsRegular() {
		fmt.Println(CSVFILE, "not a regular file!")
		return
	}

	err = readCSV(CSVFILE)
	if err != nil {
		fmt.Println(err)
		return
	}

	err = createIndex()
	if err != nil {
		fmt.Println("Cannot create index.")
		return
	}

	// Differentiating between the commands (обрабатываем переданную в качестве аргумента команду - search|list|insert|delete)
	switch args[1] {
	case "list":
		list()
	case "insert":
		if len(args) != 5 {
			fmt.Println("Usage: insert Name Surname Telephone")
			return
		}

		t, err := validTel(args[4]) // проверяем введенный номер телефона
		if err != nil {
			fmt.Println("Not a valid tel number:", args[4]) // если в номере телефона есть буквы или НЕ только цифры - выводим подсказку и завершаемся
			return
		}

		temp := initS(args[2], args[3], t)
		// If it was nil, there was an error
		if temp != nil {
			err := insert(temp)
			if err != nil {
				fmt.Println(err)
				return
			}
		}
	case "delete":
		if len(args) != 3 {
			fmt.Println("Usage: delete Number")
			return
		}
		t, err := validTel(args[2]) // проверяем введенный номер телефона
		if err != nil {
			fmt.Println("Not a valid tel number:", args[4]) // если в номере телефона есть буквы или НЕ только цифры - выводим подсказку и завершаемся
			return
		}

		err = deleteEntry(t)
		if err != nil {
			fmt.Println(err)
		}
	case "search":
		if len(args) != 3 {
			fmt.Println("Usage: search Number")
			return
		}
		t, err := validTel(args[2]) // проверяем введенный номер телефона
		if err != nil {
			fmt.Println("Not a valid tel number:", args[4]) // если в номере телефона есть буквы или НЕ только цифры - выводим подсказку и завершаемся
			return
		}

		temp := search(t)
		if temp == nil {
			fmt.Println("Number not found:", t)
			return
		}
		fmt.Println(*temp)
	default:
		if len(args) > 1 {
			fmt.Println(args[1], "- not a valid option.")
		} else {
			fmt.Println("Not a valid option.")
		}
	}
}
